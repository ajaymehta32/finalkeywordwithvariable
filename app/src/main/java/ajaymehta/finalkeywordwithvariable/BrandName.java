package ajaymehta.finalkeywordwithvariable;

/**
 * Created by Avi Hacker on 7/26/2017.
 */

public class BrandName {

    //=============  WRONG WAY ==========================

    //its just one name ..n unique for all our obejcts (all clothes) ..so if just one ..we can initlize it outside constructor..

    final String branName = "Levis";  // suppose it is our brand .. if we initlzie it outside contructs..so means we dont need to asocitate with any object..so it could have consiquence ..
    // we need brandanem associated with every object..

    // so initlzie it in a constuctor ..even if just has the same value ..doesnt matter..see BrandName2  ..for doing it..


    public static void main(String args[]) {

    }
}
