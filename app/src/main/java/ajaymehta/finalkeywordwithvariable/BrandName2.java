package ajaymehta.finalkeywordwithvariable;

/**
 * Created by Ajay Mehta on 7/26/2017.
 */



public class BrandName2 {

    final String brandName;


    public BrandName2(String brandName) {

        this.brandName = brandName;

    }


    public static void main(String args[]) {

     BrandName2 jeans = new BrandName2("Levis");  //  particular jeans with serial number
     BrandName2 tshirt = new BrandName2("Levis");

        System.out.println(jeans.brandName+" -- "+tshirt.brandName);

    }
}
