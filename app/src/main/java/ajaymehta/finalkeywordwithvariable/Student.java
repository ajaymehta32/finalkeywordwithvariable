package ajaymehta.finalkeywordwithvariable;

/**
 * Created by Ajay Mehta on 7/26/2017.
 */

// 1  .. means use constructor main banate hai to ..uski value .. constant rahegi har ek single object ke liye (apne apne object ke liye)
// 2. making a variable final means ... hume use initilze karna padega ...bina initilize kiye bina hum use nai chod sakte...
    // hum do jagah final vairable ko initlize kar sakte hai ....1) ya to filed line main  public final int =5;
    // 2.) ya to ...constructor main....

    // why we need to make a variable final ?? reason ...
    // there could be some conditions ..socho hum school project bana rhe hai ..to jitne bhi students honge usme vo sare object honnge..
    //to unke baare main or kuch info ho ya na ho ...but har kisi ka naame jarur hona chaea.... <-- means is String name  field ko use karna important hai...
    // iske bina humara code work nai karna chahea.. to ise final bana dete hai..to agar ise intilize nai karenge to ... humara program nai chalna chahea..



public class Student {
    // uncomment n see its getting red because we have not initlized it,
 //   final String name;   //  agar hum ise yaha initlize kar de to ye constant ho jayega..just one value for whole project..agar humhare pass 10 students hai to 10 object banane padege...agar yahi initlize karenge to 10 students (objects) ko kaise naam denge.

    final String name; // it will show error if you dont initilze it in constructor.. means this value in necessary for our software so ..we have to initlize it..
    int age;

    public Student(String name, int age) {

        this.name = name; // necessary    n constant for every object   // initilizing final variable..
        this.age = age;  // optional
    }


    public static void main(String args[]) {

        Student ajay = new Student("Ajay",24);
        Student rocky = new Student("Rocky",24);

        System.out.println(ajay.name+" -- "+ajay.age);

    }
}
